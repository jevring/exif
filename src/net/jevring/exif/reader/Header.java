/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.exif.reader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ScatteringByteChannel;
import java.util.Arrays;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2010-08-27 20:59:26
 */
public class Header {
    /**
     * Unfortunately we have to cast these to byte, since they are > 127.
     */
    public static final byte[] EXPECTED_JPEG_HEADER = new byte[] {(byte) 0xFF, (byte) 0xD8};
    public static final byte[] EXIF_APP_HEADER = new byte[] {(byte) 0xFF, (byte) 0xE1};
    public static final byte[] COMMENT_HEADER = new byte[] {(byte) 0xFF, (byte) 0xFE};
    public static final byte[] DHT_HEADER = new byte[] {(byte) 0xFF, (byte) 0xC4}; // start of actual JPEG data, skip it 
    public static final byte[] DQT_HEADER = new byte[] {(byte) 0xFF, (byte) 0xDB}; // start of actual JPEG data, skip it
	public static final byte[] EXPECTED_EXIF_MARKER = "Exif".getBytes();
    public static final byte[] EXPECTED_EXIF_HEADER_PADDING = new byte[] {0x00, 0x00};
    public static final byte[] EXPECTED_BIG_ENDIAN_TIFF_HEADER = new byte[] {0x00, 0x2A};
    public static final byte[] EXPECTED_LITTLE_ENDIAN_TIFF_HEADER = new byte[] {0x2A, 0x00};
    public static final byte[] II = new byte[] {0x49, 0x49}; // little endian (Intel)
    public static final byte[] MM = new byte[] {0x4D, 0x4D}; // big endian (Motorola)

    private final byte[] jpegHeader = new byte[2];
    private final byte[] appHeader = new byte[2];
    private final byte[] headerDataLength = new byte[2];
    private final byte[] exifMarker = new byte[4];
    private final byte[] exifHeaderPadding = new byte[2];
    private final byte[] byteOrderHeader = new byte[2];
    private final byte[] tiffHeaderMarker = new byte[2];
    private final byte[] offsetOfIFD = new byte[4];
    private final byte[] numberOfFieldsHeader = new byte[2];

    private final long EXPECTED_HEADER_LENGTH =
                    headerDataLength.length +
                    exifMarker.length +
                    exifHeaderPadding.length +
                    byteOrderHeader.length +
                    tiffHeaderMarker.length +
                    offsetOfIFD.length +
                    numberOfFieldsHeader.length;
    private final ByteBuffer[] headers = new ByteBuffer[] {
            ByteBuffer.wrap(headerDataLength),
            ByteBuffer.wrap(exifMarker),
            ByteBuffer.wrap(exifHeaderPadding),
            ByteBuffer.wrap(byteOrderHeader),
            ByteBuffer.wrap(tiffHeaderMarker),
            ByteBuffer.wrap(offsetOfIFD),
            ByteBuffer.wrap(numberOfFieldsHeader)
    };

    private int length;
    /**
     * Offset of the 0th IFD from the <b>start of the TIFF header</b>. An offset of 8 means that
     * the IFD starts right after the TIFF header, which is 8 bytes long.
     */
    private int offset;
	/**
	 * The number of only contains part of the truth. There's a field (34665) that is essentially 
	 * an "exif fields marker" that points to more exif fields.
	 */
    private int numberOfFields;
	private String comment;
    private ByteOrder byteOrder;

    public void read(ScatteringByteChannel sbc) throws IOException {
	    sbc.read(ByteBuffer.wrap(jpegHeader));
	    if (!Arrays.equals(jpegHeader, EXPECTED_JPEG_HEADER)) {
		    throw new IOException("Got unexpected JPEG header: " + Arrays.toString(jpegHeader));
	    }
	    // Discard all APPn headers that we don't want
	    int discardedHeaderBytes = 0;
	    while (true) {
		    sbc.read(ByteBuffer.wrap(appHeader));
		    if (Arrays.equals(appHeader, EXIF_APP_HEADER)) {
			    break;
		    } else if (appHeader(appHeader)) {
			    discardedHeaderBytes += discardHeader(sbc);
		    } else if (Arrays.equals(appHeader, COMMENT_HEADER)) {
			    comment = readComment(sbc);
		    } else if (Arrays.equals(appHeader, DHT_HEADER) || Arrays.equals(appHeader, DQT_HEADER)) {
			    // these headers indicate the start of the actual JPEG data, so we're not interested int hem
			    throw new IOException("No APP header present");
		    } else {
			    throw new IOException("Got unknown APP header: [0x" + Integer.toHexString(appHeader[0]).toUpperCase() + ", 0x" + Integer.toHexString(
					    appHeader[1]).toUpperCase() + "]");
		    }
	    }
	    
	    long bytesRead = sbc.read(headers);

        if (bytesRead != EXPECTED_HEADER_LENGTH) {
            throw new IOException("Insufficient amount of bytes read. Expecting '" + EXPECTED_HEADER_LENGTH + "', read '" + bytesRead + "'");
        }
        if (!Arrays.equals(exifMarker, EXPECTED_EXIF_MARKER)) {
            throw new IOException("Got unexpected EXIF header: " + Arrays.toString(exifMarker));
        }
        if (!Arrays.equals(exifHeaderPadding, EXPECTED_EXIF_HEADER_PADDING)) {
            throw new IOException("Got unexpected EXIF header padding: " + Arrays.toString(exifHeaderPadding));
        }
        if (Arrays.equals(byteOrderHeader, II)) {
            byteOrder = ByteOrder.LITTLE_ENDIAN;
        } else if (Arrays.equals(byteOrderHeader, MM)) {
            byteOrder = ByteOrder.BIG_ENDIAN;
        } else {
            throw new IOException("Got unexpected byte order marker: " + Arrays.toString(byteOrderHeader));
        }
        if (!Arrays.equals(tiffHeaderMarker, byteOrder == ByteOrder.BIG_ENDIAN ? EXPECTED_BIG_ENDIAN_TIFF_HEADER : EXPECTED_LITTLE_ENDIAN_TIFF_HEADER)) {
            throw new IOException("Got unexpected TIFF header: " + Arrays.toString(tiffHeaderMarker));
        }

        // do & 0xff (short) 0xffff (int) to handle the fact that bytes are normally unsigned.
        // http://stackoverflow.com/questions/11088/what-is-the-best-way-to-work-around-the-fact-that-all-java-bytes-are-signed/15657#15657
        offset = (ByteBuffer.wrap(offsetOfIFD).order(byteOrder).getInt() & 0xFFFF) + discardedHeaderBytes;
        numberOfFields = ByteBuffer.wrap(numberOfFieldsHeader).order(byteOrder).getShort() & 0xFFFF;
        length = ByteBuffer.wrap(headerDataLength).order(byteOrder).getShort() & 0xFFFF;
    }

	private boolean appHeader(byte[] appHeader) {
		if (appHeader[0] == 0xFFFFFFFF) {
			if (appHeader[1] >= 0xFFFFFFE0 && appHeader[1] <= 0xFFFFFFEF) {
				return true;
			}
		}
		return false;
	}

	private int discardHeader(ScatteringByteChannel sbc) throws IOException {
		byte[] length = new byte[2];
		sbc.read(ByteBuffer.wrap(length)); // find out how much data we can skip
		int headerLengthIncludingLengthField = ByteBuffer.wrap(length).getShort() & 0xFFFF;
		int remainingBytes = headerLengthIncludingLengthField - 2;
		byte[] remainingHeaderBytes = new byte[remainingBytes];
		sbc.read(ByteBuffer.wrap(remainingHeaderBytes)); // discard the data
		//System.out.println("Skipped " + remainingBytes + " bytes of remaining header: '" + new String(remainingHeaderBytes) + "'");
		return headerLengthIncludingLengthField + 2; // the length plus the APPn identifier
	}

	private String readComment(ScatteringByteChannel sbc) throws IOException {
		byte[] length = new byte[2];
		sbc.read(ByteBuffer.wrap(length)); // find out how long the comment is
		int commentLengthIncludingLengthField = ByteBuffer.wrap(length).getShort() & 0xFFFF;
		int remainingBytesLength = commentLengthIncludingLengthField - 2;
		byte[] remainingBytes = new byte[remainingBytesLength];
		sbc.read(ByteBuffer.wrap(remainingBytes)); // read the comment
		return new String(remainingBytes);
	}

	public int getLength() {
        return length;
    }

    public int getOffset() {
        return offset;
    }

    public int getNumberOfFields() {
        return numberOfFields;
    }

    public ByteOrder getByteOrder() {
        return byteOrder;
    }

	public String getComment() {
		return comment;
	}

	@Override
    public String toString() {
        return "Header{" +
                "length=" + length +
                ", offset=" + offset +
                ", numberOfFields=" + numberOfFields +
                ", byteOrder=" + byteOrder +
                ", comment=" + comment +
                '}';
    }
}
