/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.exif.reader;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ScatteringByteChannel;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a 12 byte long header field.
 * @author markus@jevring.net
 * @version $Id$
 * @since 2010-08-27 22:53:56
 */
public class Field {
    // todo: we might need more information from this field later. Perhaps we should refactor it. Do that when needed.
    private static final Map<Integer, Integer> BYTES_PER_COUNT = new HashMap<Integer, Integer>() {{
	    // todo: we encounter type=0 and type=8 and type=32 as well. What are those, and what should their value be?
	    // type=8 for ISOSpeedRatings 
	    // type=32 for Make
	    // todo: source a link for these values
	    
        // comments from the EXIF spec.
        put(1, 1); // BYTE  An 8-bit unsigned integer.,
        put(2, 1); // ASCII  An 8-bit byte containing one 7-bit ASCII code. The final byte is terminated with NULL.,
        put(3, 2); // SHORT  A 16-bit (2-byte) unsigned integer,
        put(4, 4); // LONG  A 32-bit (4-byte) unsigned integer,
        put(5, 8); // RATIONAL  Two LONGs. The first LONG is the numerator and the second LONG expresses the denominator.,
        put(7, 1); // UNDEFINED  An 8-bit byte that can take any value depending on the field definition,
        put(9, 4); // SLONG  A 32-bit (4-byte) signed integer (2's complement notation),
        put(10, 8); // SRATIONAL  Two SLONGs. The first SLONG is the numerator and the second SLONG is the denominator.
    }};
    private static final Map<Integer, String> TAG_NAMES = new HashMap<Integer, String>() {{
        // http://owl.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html
	    // http://www.exiv2.org/tags.html

        // 0th IFD TIFF Tags
        put(256, "ImageWidth");
        put(257, "ImageLength");
        put(258, "BitsPerSample");
        put(259, "Compression");
        put(262, "PhotometricInterpretation");
        put(270, "ImageDescription");
        put(271, "Make");
        put(272, "Model");
        put(273, "StripOffsets");
        put(274, "Orientation");
        put(277, "SamplesPerPixel");
        put(278, "RowsPerStrip");
        put(279, "StripByteCounts");
        put(282, "XResolution");
        put(283, "YResolution");
        put(284, "PlanarConfiguration");
        put(296, "ResolutionUnit");
        put(301, "TransferFunction");
        put(305, "Software");
        put(306, "DateTime");
        put(315, "Artist");
        put(318, "WhitePoint");
        put(319, "PrimaryChromaticities");
        put(513, "JPEGInterchangeFormat");
        put(514, "JPEGInterchangeFormatLength");
        put(529, "YCbCrCoefficients");
        put(530, "YCbCrSubSampling");
        put(531, "YCbCrPositioning");
        put(532, "ReferenceBlackWhite");
        put(33432, "Copyright");
        put(34665, "Exif IFD Pointer");
        put(34853, "GPSInfo IFD Pointer");

        // 0th IFD Exif Private Tags
        put(33434, "ExposureTime");
        put(33437, "FNumber");
        put(34850, "ExposureProgram");
        put(34852, "SpectralSensitivity");
        put(34855, "ISOSpeedRatings");
        put(34856, "OECF");
        put(36864, "EXIFVersion");
        put(36867, "DateTimeOriginal");
        put(36868, "DateTimeDigitized");
        put(37121, "ComponentsConfiguration");
        put(37122, "CompressedBitsPerPixel");
        put(37377, "ShutterSpeedValue");
        put(37378, "ApertureValue");
        put(37379, "BrightnessValue");
        put(37380, "ExposureBiasValue");
        put(37381, "MaxApertureValue");
        put(37382, "SubjectDistance");
        put(37383, "MeteringMode");
        put(37384, "LightSource");
        put(37385, "Flash");
        put(37386, "FocalLength");
        put(37500, "MakerNote");
        put(37510, "UserComment");
        put(37520, "SubSecTime");
        put(37521, "SubSecTimeOriginal");
        put(37522, "SubSecTimeDigitized");
        put(40960, "FlashPixVersion");
        put(40961, "ColorSpace");
        put(40962, "PixelXDimension");
        put(40963, "PixelYDimension");
        put(40964, "RelatedSoundFile");
        put(40965, "Interoperability IFD Pointer");
        put(41483, "FlashEnergy");
        put(41484, "SpecialFrequencyResponse");
        put(41486, "FocalPlaneXResolution");
        put(41487, "FocalPlaceYResolution");
        put(41488, "FocalPlaneResolutionUnit");
        put(41492, "SubjectLocation");
        put(41493, "ExposureIndex");
        put(41495, "SensingMethod");
        put(41728, "FileSource");
        put(41729, "SceneType");
        put(41730, "CFAPattern");

        put(41985, "CustomRendered");
        put(41986, "ExposureMode");
        put(41987, "WhiteBalance");
        put(41988, "DigitalZoomRatio");
        put(41989, "FocalLengthIn35mmFormat");
        put(41990, "SceneCaptureType");
        put(41991, "GainControl");
        put(41992, "Contrast");
        put(41993, "Saturation");
        put(41994, "Sharpness");
        put(41995, "DeviceSettingDescription");
        put(41996, "SubjectDistanceRange");
        put(41730, "CFAPattern");
        
	    put(42035, "LensMake");
        put(42036, "LensModel");
        put(42037, "LensSerialNumber");


	    // 0th IFD GPS Info Tags
        put(0, "GPSVersionID");
        put(1, "GPSLatitudeRef");
        put(2, "GPSLatitude");
        put(3, "GPSLongitudeRef");
        put(4, "GPSLongitude");
        put(5, "GPSAltitudeRef");
        put(6, "GPSAltitude");
        put(7, "GPSTimeStamp");
        put(8, "GPSSatellites");
        put(9, "GPSStatus");
        put(10, "GPSMeasureMode");
        put(11, "GPSDOP");
        put(12, "GPSSpeedRef");
        put(13, "GPSSpeed");
        put(14, "GPSTrackRef");
        put(15, "GPSTrack");
        put(16, "GPSImgDirectionRef");
        put(17, "GPSImgDirection");
        put(18, "GPSMapDatum");
        put(19, "GPSDestLatitudeRef");
        put(20, "GPSDestLatitude");
        put(21, "GPSDestLongitudeRef");
        put(22, "GPSDestLongitude");
        put(23, "GPSDestBearingRef");
        put(24, "GPSDestBearing");
        put(25, "GPSDestDistanceRef");
        put(26, "GPSDestDistance");

        
    }};
    private final byte[] tagData = new byte[2];
    private final byte[] typeData = new byte[2];
    private final byte[] countData = new byte[4];
    private final byte[] valueOrOffsetData = new byte[4];
    private final ByteBuffer[] parts = new ByteBuffer[] {
            ByteBuffer.wrap(tagData),
            ByteBuffer.wrap(typeData),
            ByteBuffer.wrap(countData),
            ByteBuffer.wrap(valueOrOffsetData)
    };
    private final ByteOrder byteOrder;

    private int tag;
    private int type;
    private int count;
    private int valueOrOffset;

    public Field(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    public void read(ScatteringByteChannel sbc) throws IOException {
        sbc.read(parts);

        // do & 0xff (short) 0xffff (int) to handle the fact that bytes are normally unsigned.
        // http://stackoverflow.com/questions/11088/what-is-the-best-way-to-work-around-the-fact-that-all-java-bytes-are-signed/15657#15657
        tag = ByteBuffer.wrap(tagData).order(byteOrder).getShort() & 0xFFFF;
        type = ByteBuffer.wrap(typeData).order(byteOrder).getShort() & 0xFFFF;
        count = ByteBuffer.wrap(countData).order(byteOrder).getInt() & 0xFFFF;
        valueOrOffset = ByteBuffer.wrap(valueOrOffsetData).order(byteOrder).getInt() & 0xFFFF;
    }

    public int getTag() {
        return tag;
    }

    public String getName() {
        String tagName = TAG_NAMES.get(tag);
        if (tagName != null) {
            return tagName;
        } else {
            return String.valueOf(tag);
        }
    }

    public String getDescription() {
        String tagName = TAG_NAMES.get(tag);
        if (tagName != null) {
            return tagName + " (" + tag + ")";
        } else {
            return String.valueOf(tag);
        }
    }

    public int getType() {
        return type;
    }

    public int getCount() {
        return count;
    }

    /**
     * If the data is smaller than 4 bytes (as described by {@link #getDataLength()},
     * the data is actually contained in this variable, rathern than pointing to an offset.
     *
     * @return the value if {@link #getDataLength()} <= 4, otherwise the offset from the
     * TIFF header where to look for the data.
     */
    public int getValueOrOffset() {
        return valueOrOffset;
    }

    public byte[] getValueOrOffsetData() {
        return valueOrOffsetData;
    }

    /**
     * This value represents how much data to be read from file to get all the data
     * in this field. This is calculated by multiplying the {@link #getCount() count}
     * with the number of bytes per count for this {@link #getType() type} of field.
     *
     * @return the number of bytes to read to get all the data in this field.
     * @see #isBytesPerCountKnownForType() 
     */
    public int getDataLength() {
        return count * BYTES_PER_COUNT.get(type);
    }
	
	public boolean isBytesPerCountKnownForType() {
		return BYTES_PER_COUNT.get(type) != null;
	}

    @Override
    public String toString() {
        return "Field{" +
                "tag=" + tag +
                ", type=" + type +
                ", count=" + count +
                ", valueOffset=" + valueOrOffset +
                ", dataLength=" + (isBytesPerCountKnownForType() ? getDataLength() : "Unknown") +
                ", tagName=" + TAG_NAMES.get(tag) +
                '}';
    }

    /**
     * Provides the value of this field, based on the data provided,
     * in whatever format the field is in.
     * @param data the data read for this field.
     * @return the value of the data in the shape described by this field,
     * based on its {@link #getType() type}. If this type is UNDEFINED,
     * the provided byte array will be returned.
     */
    public Object valueOf(byte[] data) {
        // todo: in fact, all these need to return the value of the whole buffer.
        // this is especially important with UNKNOWN and ASCII,
        // but basically anywhere where we have a dataLength > data size

        ByteBuffer buffer = ByteBuffer.wrap(data).order(byteOrder);
        int numerator, denominator;
        switch (type) {
            case 10:
                // don't do & 0xFFFF here, as this is supposed to be a signed integer
                numerator = buffer.getInt();
                denominator = buffer.getInt();
                return (double)numerator / (double) denominator;
            case 9:
                // don't do & 0xFFFF here, as this is supposed to be a signed integer
                return buffer.getInt();
            case 7:
                // unknown, return all the data
                return data;
            case 5:
                numerator = buffer.getInt() & 0xFFFF;
                denominator = buffer.getInt() & 0xFFFF;
                return (double)numerator / (double)denominator;
            case 4:
                return buffer.getInt() & 0xFFFF;
            case 3:
                return buffer.getShort() & 0xFFFF;
            case 2:
	            // this is a null-terminated string, so we have to drop the last character
	            return new String(data, 0, data.length - 1);
            case 1:
                // 8-bit byte represents an integer
                return (int) buffer.get() & 0xFF;

        }
        System.err.println("returning null for type " + type);
        return null;
    }
}
