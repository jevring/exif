/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.exif;

import net.jevring.exif.info.FieldInfo;
import net.jevring.exif.info.Image;
import net.jevring.exif.reader.Field;
import net.jevring.exif.reader.Header;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2010-08-27 18:58:44
 */
public class ExifReader {
    private Image readImage(File jpegFile, boolean onlyDesirableAttributes) throws IOException {
        try (RandomAccessFile raf = new RandomAccessFile(jpegFile, "r")) {
	        FileChannel fileChannel = raf.getChannel();
            Header header = new Header();
            header.read(fileChannel);
            //System.out.println("header = " + header);

            Integer exifFieldsAddress = -1;

            // read the TIFF fields
            List<Field> fields = new ArrayList<>();
            for (int i = 0; i < header.getNumberOfFields(); i++) {
                Field field = new Field(header.getByteOrder());
                field.read(fileChannel);
	            if (field.isBytesPerCountKnownForType()) {
	                fields.add(field);
	                if (field.getTag() == 34665) {
		                // todo: change this so that we can dive in to all IFDs in a recursive manner
		                // todo: are the offsets different for each IFD, or are they still file offsets?
		                exifFieldsAddress = (Integer) field.valueOf(field.getValueOrOffsetData());
	                }
	            } else {
		            System.err.println("Bytes per count not known for field " + field);
	            }
            }

            if (exifFieldsAddress != -1) {
                raf.seek(4 + header.getOffset() + exifFieldsAddress);
                byte[] exifAddressData = new byte[2];
                fileChannel.read(ByteBuffer.wrap(exifAddressData));
                int numberOfExifFields = ByteBuffer.wrap(exifAddressData).order(header.getByteOrder()).getShort() & 0xFFFF;
                for (int i = 0; i < numberOfExifFields; i++) {
                    Field field = new Field(header.getByteOrder());
                    field.read(fileChannel);
	                if (field.isBytesPerCountKnownForType()) {
                        fields.add(field);
	                } else {
		                System.err.println("Bytes per count not known for field " + field);
	                }
                }
            }

            Image image = new Image(jpegFile);

            // read the TIFF field values
            for (Field field : fields) {
                /*
                A comment in the spec mentions that values are:
                "Value longer than 4byte of 0th IFD". Perhaps that is why.
                 */
                if (onlyDesirableAttributes && !image.isDesiredAttribute(field.getTag())) {
                    // don't read attributes that we don't want anyway
                    continue;
                }

                byte[] data;
                if (field.getDataLength() <= 4) {
                    data = field.getValueOrOffsetData();
                } else {
                    raf.seek(4 + header.getOffset() + field.getValueOrOffset());
                    data = new byte[field.getDataLength()];
                    fileChannel.read(ByteBuffer.wrap(data));
                }
                image.addInformation(new FieldInfo(field.getTag(), field.getType(), field.getName(), field.valueOf(data)));
            }
            return image;
        }
    }

	private static Map<String, List<Image>> splitByCamera(Collection<Image> images) {
		Map<String, List<Image>> map = new HashMap<>();
		for (Image image : images) {
			String key = image.getTagValue(Image.CAMERA_MAKE) + " - " + image.getTagValue(Image.CAMERA_MODEL);
			if (image.getTagValue(Image.LENS_MODEL) != null && image.getTagValue(Image.LENS_MODEL) != null) {
				key += "[" + image.getTagValue(Image.LENS_MODEL) + " - " + image.getTagValue(Image.LENS_MODEL) + "]";
			}
			List<Image> list = map.get(key);
			if (list == null) {
				list = new ArrayList<>();
				map.put(key, list);
			}
			list.add(image);
		}
		return map;
	}

	private static void printFocalLengthStatistics(Collection<Image> images) {
        Map<Double, Long> focalLengths = new HashMap<>();
        for (Image image : images) {
            Double focalLength = (Double) image.getTagValue(Image.FOCAL_LENGTH);
            if (focalLength != null) {
                Long count = focalLengths.get(focalLength);
                if (count == null) {
                    count = 0L;
                }
                focalLengths.put(focalLength, count + 1);
            }
        }
        for (Map.Entry<Double, Long> focalLengthCount : focalLengths.entrySet()) {
	        Double focalLength = focalLengthCount.getKey();
	        Long imagesAtThisFocalLength = focalLengthCount.getValue();
	        System.out.printf("%5.1f\t%5.2f%%\t%5d/%5d\n",
	                          focalLength,
	                          (imagesAtThisFocalLength / (double) images.size()) * 100,
	                          imagesAtThisFocalLength,
	                          images.size());
        }
    }

    public static void main(String[] args) {
	    if (args.length == 0) {
		    System.err.println("Provide either a jpg or a path as a parameter. Single files get their EXIF data dumped, directories generate statistics.");
		    return;
	    }
        
        try {
            final ExifReader exifReader = new ExifReader();
            final File file = new File(args[0]);
            if (file.isDirectory()) {
	            System.out.println("start: " + new Date());
	            long start = System.currentTimeMillis();
	            final List<Path> files = new ArrayList<>();
	            final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	            Files.walkFileTree(Paths.get(file.getAbsolutePath()), new SimpleFileVisitor<Path>() {
		            @Override
		            public FileVisitResult visitFile(final Path path, BasicFileAttributes attrs) throws IOException {
			            File file = path.toFile();
			            if (file.isFile() && file.getName().toLowerCase().endsWith(".jpg")) {
			                files.add(path);
			            }
			            return super.visitFile(path, attrs);
		            }
	            });
	            final CountDownLatch latch = new CountDownLatch(files.size());
	            final BlockingQueue<Image> images = new LinkedBlockingQueue<>();
	            final ConcurrentMap<String, AtomicInteger> errorMessages = new ConcurrentHashMap<>();
	            for (final Path path : files) {
		            executor.execute(new Runnable() {
			            @Override
			            public void run() {
				            try {
					            images.add(exifReader.readImage(path.toFile(), true));
				            } catch (IOException e) {
					            if (e.getMessage().equals("Got unexpected JPEG header: [-1, -40, -1, -32]")) {
						            System.out.println("Wrong JPEG header for: " + path);
					            }
					            AtomicInteger counter = errorMessages.get(e.getMessage());
					            if (counter == null) {
						            AtomicInteger newCounter = new AtomicInteger();
						            counter = errorMessages.putIfAbsent(e.getMessage(), newCounter);
						            if (counter == null) {
							            counter = newCounter;
						            }
					            }
					            counter.incrementAndGet();
				            } finally {
					            latch.countDown();
				            }
			            }
		            });
	            }
	            latch.await();
	            executor.shutdownNow();
	            
	            long end = System.currentTimeMillis();
	            System.out.println("Found " + files.size() + " files in " + ((end - start) / 1000d) + " seconds");
	            System.out.println("Errors: ");
	            for (Map.Entry<String, AtomicInteger> entry : errorMessages.entrySet()) {
		            System.out.println(entry.getKey() + ": " + entry.getValue().get());
	            }
	            Map<String, List<Image>> imagesPerCamera = splitByCamera(images);
	            for (Map.Entry<String, List<Image>> entry : imagesPerCamera.entrySet()) {
		            System.out.println(entry.getKey());
		            List<Image> imagesForCamera = entry.getValue();
		            printFocalLengthStatistics(imagesForCamera);
	            }
	            System.out.println("end: " + new Date());
            } else {
                Image image = exifReader.readImage(file, false);
	            System.out.println("Image: " + file.getAbsolutePath());
	            List<String> output = new ArrayList<>();
	            for (Map.Entry<Integer, FieldInfo> entry : image.getFields().entrySet()) {
		            output.add(String.format("%5d - %s", entry.getKey(), entry.getValue()));
	            }
	            Collections.sort(output);
	            for (String line : output) {
		            System.out.println(line);
	            }
            }
	        
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
	    
    }
}
