/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.exif.info;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2010-08-28 18:47:16
 */
public class Image {
    public static final int APERTURE = 33437;
    public static final int ISO = 34855;
    public static final int SHUTTER_SPEED = 33434;
    public static final int FOCAL_LENGTH = 37386;
    public static final int CAMERA_MAKE = 271;
    public static final int CAMERA_MODEL = 272;
	public static final int FOCAL_LENGTH_IN_35MM_FORMAT = 41989;
	public static final int LENS_MAKE = 42035;
	public static final int LENS_MODEL = 42036;
	public static final Set<Integer> desiredAttributes = new HashSet<Integer>() {{
		add(APERTURE);
		add(ISO);
		add(SHUTTER_SPEED);
		add(FOCAL_LENGTH);
		add(CAMERA_MAKE);
		add(CAMERA_MODEL);
		add(FOCAL_LENGTH_IN_35MM_FORMAT);
		add(LENS_MAKE);
		add(LENS_MODEL);
	}};

    private final Map<Integer, FieldInfo> information = new HashMap<>();
    private final File file;

    public Image(File file) {
        this.file = file;
    }

    public void addInformation(FieldInfo fieldInfo) {
        information.put(fieldInfo.getTag(), fieldInfo);
    }

    /**
     * Checks whether we want this information is wanted in the file or not.
     * By checking this externally, we can avoid reading many fields that
     * we don't want, which speeds things up.
     * @param tag the tag of the field.
     * @return {@code true} if we are interested in the information, {@code false} otherwise.
     */
    public boolean isDesiredAttribute(int tag) {
        return desiredAttributes.contains(tag);
    }

    public Object getTagValue(int tag) {
        FieldInfo fieldInfo = information.get(tag);
        if (fieldInfo == null) {
            return null;
        } else {
            return fieldInfo.getValue();
        }
    }

	public Map<Integer, FieldInfo> getFields() {
		return information;
	}

	@Override
    public String toString() {
        return "Image{" +
                "file=" + file +
                ", information=" + information +
                '}';
    }
}
