/*
 * Copyright (c) 2014, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.exif.info;

import java.util.Arrays;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2010-08-28 18:44:56
 */
public class FieldInfo {
    private final int tag;
    private final int type;
    private final String name;
    private final Object value;

    public FieldInfo(int tag, int type, String name, Object value) {
        this.tag = tag;
        this.type = type;
        this.name = name;
        this.value = value;
    }

    public int getTag() {
        return tag;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        String stringValue = String.valueOf(value);
	    if (type == 7) {
		    // this is occasionally a string, but mostly gibberish,
		    // so we can't just turn it into a string and hope for the best
		    stringValue = Arrays.toString((byte[]) value);
	    } else if (tag == Image.SHUTTER_SPEED && value != null) {
            Double v = (Double) value;
            if (v < 1.0d) {
                stringValue = "1/" + (int)(1.0 / v);    
            }
        }
        return "{" + name + "=" + stringValue + "}";
    }
}
